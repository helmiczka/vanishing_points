#ifndef HoughWindow_H_
#define HoughWindow_H_

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <qslider.h>
#include "QOpenCVWidget.h"
#include <opencv2\opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include "Vanish.h"


#include<QMessageBox>
#include<QKeyEvent>
#include <qfiledialog.h>
#include <qpushbutton.h>


class HoughWindow : public QWidget
{
    Q_OBJECT
    private:
        QOpenCVWidget *cvwidget;
		QLabel *sliderV;
		Vanish v;
		void keyPressEvent(QKeyEvent* e);
        
    public:
       HoughWindow(QWidget *parent=0);
protected slots:
	   void setThresh(int);
	   void openFile();
	   void next();
	   void prev();
	   void Detect();
	   void showVal(int);
         
    protected:     
};


#endif
