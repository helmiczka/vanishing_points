//openCV
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <opencv2\opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <vector>

#include "Detect.h"

#pragma once

using namespace cv;
using namespace std;

class Vanish
{

	vector<Mat> images;
	vector<Mat>::iterator imgit;

public:
	Vanish(void);
	~Vanish(void);
	void init();
	void loadSample(const string & filename);
	Detect detector;

	void setImg(bool right);
	void resetImg() {
		if (!images.empty()) 
			imgit->copyTo(image);
	};
	Mat image;
	
	// tento pochaby callback musi byt public, alebo static. Public ma zmysel
	void mouse_callback(int event, int x, int y, int flags);
	void run();
	void outputData();
};