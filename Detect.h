
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <opencv2\opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <vector>

#pragma once

using namespace cv;
using namespace std;


class Detect
{
	int hough_thresh;
	
	vector<vector< double > > general_lines;


public:
	Detect(void);
	~Detect(void);

	Mat detect(Mat & input);

	void findHoughLines(Mat & input);

	void intersections2( Mat & output);
	
	void setThresh(int thresh) {
		this->hough_thresh = thresh;
	};

};

