// geom.cpp : main project file.

#include "stdafx.h"
#include <iostream>

#include <QApplication>
#include <QWidget>
#include <QVBoxLayout>
#include "QOpenCVWidget.h"
#include "HoughWindow.h"

#include "Vanish.h"

using namespace std;

int main(int argc, char**argv)
{	
	QApplication app(argc, argv);
	HoughWindow *mainWin = new HoughWindow();
    mainWin->setWindowTitle("VGE - vanishing point detection");
    mainWin->show();    
    int retval = app.exec();

    return 0;
}
