#include "StdAfx.h"
#include "Vanish.h"
#include <time.h>
#include <iostream>
#define NOMINMAX
#include <windows.h>

#define SNAPSHOTS

Vanish::Vanish(void)
{
}


Vanish::~Vanish(void)
{
}

// callback has to be static
// http://stackoverflow.com/questions/6489457/cvsetmousecallback-in-opencv-2-1-managed-c-cli-c
//http://stackoverflow.com/questions/6781622/opencv-2-3-with-vs-2008-mouse-events
void mouse_callback_wrapper(int event, int x, int y, int flags, void* ptr){
	Vanish* uselessWrapperPointer = (Vanish*)ptr;
	if (uselessWrapperPointer)
		uselessWrapperPointer->mouse_callback(event,  x,  y,  flags);

}

void Vanish::loadSample(const string & filename)
{
	images.push_back(imread(filename));
	cout <<images.size();
	imgit = images.begin();
	setImg(true);

}


void Vanish::setImg(bool right)
{
	if (images.empty()) return;
	if (right){
		if (imgit != (images.end()-1)) imgit++;
	}
	else {
		if (imgit != images.begin()) imgit--;
	}
	imgit->copyTo(image);
}

void Vanish::mouse_callback(int event, int x, int y, int flags){
	
	switch( event ){
		case CV_EVENT_LBUTTONUP:				
				detector.detect(image).copyTo(image );
		break;
	}
}
