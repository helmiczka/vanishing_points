========================================================================
    APPLICATION : Vanishing point detector
    AUTHOR: Samuel Mudr�k

Faculty of Information Technology of Brno University of Technology, 2013
========================================================================

Requirements:
Visual Studio 2010
Qt 5.
OpenCV 2.3

![picture](vge_screen8.PNG)
