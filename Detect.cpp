#include "StdAfx.h"
#include "Detect.h"

Detect::Detect(void)
{
	hough_thresh = 100;
}

Detect::~Detect(void)
{
}

void Detect::intersections2(Mat & output)
{
	Mat acc = Mat::zeros(output.rows, output.cols, CV_8U);

	vector<vector< double > > & lines = general_lines;

	cout << "General equation lines: " << lines.size() << endl;

	for( size_t i = 0; i < lines.size(); i++ ){
		for( size_t j = 0; j < lines.size(); j++ ){
			double det = lines[i][0] * lines[j][1] - lines[j][0] * lines[i][1];
			int x = (int) ((lines[j][1] * lines[i][2]  - lines[i][1] * lines[j][2] ) / det);
			int y = (int) ((lines[i][0] * lines[j][2]  - lines[j][0] * lines[i][2] ) / det);
			if ( (x > 0) && (y > 0) &&  (x < acc.cols) && (y < acc.rows))
				acc.at<char>(y,x) += 250/lines.size();
			circle(output, Point(x,y), 4, Scalar(0,150,0));
		}
	}

	Point vanpt;
	double max_intersections = 0.0;

	::blur(acc, acc, Size(15,15));
	minMaxLoc(acc, NULL, &max_intersections, NULL, &vanpt);

	cout << " max " << max_intersections << endl;

	if (max_intersections > 0)
		circle(output, vanpt, 5, Scalar(200,0,0), 10, 8, 0);

	/*try {
	::cvtColor(acc, output, ::CV_GRAY2BGR);
		}
	catch( cv::Exception& e )
	{
		const char* err_msg = e.what();
		std::cout << "exception caught: " << err_msg << std::endl;
	}*/

}


void Detect::findHoughLines(Mat & output){
	general_lines.clear();
	vector<Vec2f> lines;
	::HoughLines(output, lines, 1, CV_PI/180, hough_thresh);
		
	::cvtColor(output, output, ::CV_GRAY2BGR);
	for( size_t i = 0; i < lines.size(); i++ )
	  {
		 float rho = lines[i][0], theta = lines[i][1];
		 Point pt1, pt2;
		 double a = cos(theta), b = sin(theta);
		 double x0 = a*rho, y0 = b*rho;
		 
		 double c = b*y0+a*x0;
		 if (lines.size() < 50) {
			 //cout << "ciara " << i << " rho " << rho << " theta " << theta << endl;
			 //cout << " x0, y0 " << x0 << " " << y0 <<endl;
			 //cout << " a b c " << a << " " << b << " " << c << endl; 
		 }

		 double gl1[] = {a,b, c};
		 vector<double> gl2(gl1, gl1 + 4*sizeof(double));
		 general_lines.push_back(gl2);

		 pt1.x = cvRound(x0 + 1000*(-b));
		 pt1.y = cvRound(y0 + 1000*(a));
		 pt2.x = cvRound(x0 - 1000*(-b));
		 pt2.y = cvRound(y0 - 1000*(a));
		 line( output, pt1, pt2, Scalar(0,0,255), 1, CV_AA);
	  }
}


Mat Detect::detect(Mat & original){
	vector<vector<Point> > foundContours;
	
	Mat output(original);

	try {
		cvtColor(output, output, ::CV_BGR2GRAY);
		::Canny(output, output, 50, 150);
		findHoughLines(output);

		intersections2(output);
	}
	catch( cv::Exception& e )
	{
		const char* err_msg = e.what();
		std::cout << "exception caught: " << err_msg << std::endl;
	}
	
	return output;
}
