#include "StdAfx.h"
#include "QOpenCVWidget.h"

// Constructor
QOpenCVWidget::QOpenCVWidget(QWidget *parent) : QWidget(parent) {
    layout = new QVBoxLayout;
    imagelabel = new QLabel;
    layout->addWidget(imagelabel);

    setLayout(layout);
}

QOpenCVWidget::~QOpenCVWidget(void) {
    
}

void QOpenCVWidget::putImage2(const Mat & cv2image) {
	if (cv2image.empty()) return;

	//cout << cv2image.rows << " " << cv2image.type() << endl;

	Mat temp;
	cvtColor(cv2image, temp, ::CV_BGR2RGB);
	QImage img((uchar*)temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
	imagelabel->setPixmap(QPixmap::fromImage(img));

}

