
#ifndef QOPENCVWIDGET_H
#define QOPENCVWIDGET_H

#include <opencv2\opencv.hpp>
#include <opencv2/core/core.hpp>
#include <QPixmap>
#include <QLabel>
#include <QWidget>
#include <QVBoxLayout>
#include <QImage>

using namespace cv;

class QOpenCVWidget : public QWidget {
    private:
        QLabel *imagelabel;
        QVBoxLayout *layout;
        
        QImage image;
        
    public:
        QOpenCVWidget(QWidget *parent = 0);
        ~QOpenCVWidget(void);
        void putImage2(const Mat &);
}; 

#endif
