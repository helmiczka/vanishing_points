#include "stdafx.h"
#include "HoughWindow.h"

using namespace cv;

void HoughWindow::setThresh(int thresh){
	v.detector.setThresh(thresh);
}

void HoughWindow::showVal(int val){
	cout << QString::number(val,10).toInt() << endl;
	sliderV->setText(QString::number(val,10));
}

void HoughWindow::openFile(){
	QString fileName;
	cout << "opening";
	fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                            "",
                                            tr("Pictures (*.jpg *.png)"));
		 
	cout<< fileName.length() << fileName.toLocal8Bit().constData() << " opened" << endl;
	v.loadSample(fileName.toLocal8Bit().constData());
	cvwidget->putImage2(v.image);
}

void HoughWindow::next(){
	cout << "right";
	v.setImg(true);
	cvwidget->putImage2(v.image);
}

void HoughWindow::prev(){
	cout << "left";
	v.setImg(false);
	cvwidget->putImage2(v.image);
}

void HoughWindow::Detect(){
	v.resetImg();
	v.image = v.detector.detect(v.image);
	cvwidget->putImage2(v.image);
}

HoughWindow::HoughWindow( QWidget *parent) : QWidget(parent) {



    QVBoxLayout *layout = new QVBoxLayout;
    QHBoxLayout *leftPanel = new QHBoxLayout;

    cvwidget = new QOpenCVWidget(this);
	QPushButton* openFileButton = new QPushButton("Open file");
	QPushButton* nextButton = new QPushButton("Next");
	QPushButton* prevButton = new QPushButton("Prev");
	QPushButton* detButton = new QPushButton("Detect");
	QSlider *slider = new QSlider(Qt::Horizontal);
	sliderV = new QLabel("100");
	slider->setRange(50,500);
	slider->setFocusPolicy(Qt::NoFocus);
	cvwidget->setFocusPolicy(Qt::StrongFocus);

	connect(slider, SIGNAL(valueChanged(int)), this, SLOT(setThresh(int)));
	connect(slider, SIGNAL(valueChanged(int)), this, SLOT(showVal(int)));
	connect(openFileButton, SIGNAL(clicked()), this, SLOT(openFile()));
	connect(nextButton, SIGNAL(clicked()), this, SLOT(next()));
	connect(prevButton, SIGNAL(clicked()), this, SLOT(prev()));
	connect(detButton, SIGNAL(clicked()), this, SLOT(Detect()));

	leftPanel->addWidget(openFileButton);
	leftPanel->addWidget(prevButton);
	leftPanel->addWidget(nextButton);
	leftPanel->addWidget(detButton);
	leftPanel->addWidget(slider);	
	leftPanel->addWidget(sliderV);

	layout->addLayout(leftPanel);

    layout->addWidget(cvwidget);
    setLayout(layout);
    resize(500, 400);

	
	slider->setValue(100);

	//v.init();
	cvwidget->putImage2(v.image);
 }

void HoughWindow::keyPressEvent(QKeyEvent* e) {
	cout << e->key() << endl;

	switch (e->key())
	{
	case Qt::Key_O: 
		openFile();
		break;
	case Qt::Key_Left:
		prev();
		break;
	case Qt::Key_Right: 
		next();
		break;
	case Qt::Key_D:
		Detect();
		break;
	default: break;
	}
}
